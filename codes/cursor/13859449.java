/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
            if (n <= 1) {
            return;
        }
        
        for (int i = 0; i < n; i++) {
            // 提前退出冒泡循环的标志位
            boolean flag = false;
            
            for (int j = 0; j < n - i - 1; j++) {
                if (a[j] > a[j + 1]) { // 将相邻元素进行比较
                    // 交换相邻元素
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    // 设置标志位为true
                    flag = true;
                }
            }
            
            if (!flag) { // 如果一次冒泡循环中没有发生交换，则说明数组已经有序，直接退出循环
                break;
            }
        }

} //end
