/**
 * 冒泡排序函数
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    if (a == null || n <= 1) {
        return; // 如果数组为空或只有一个元素，不需要排序
    }
    boolean swapped;
    for (int i = 0; i < n - 1; i++) {
        swapped = false;
        for (int j = 0; j < n - 1 - i; j++) {
            if (a[j] > a[j + 1]) {
                // 交换元素
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                swapped = true;
            }
        }
        // 如果这一轮没有发生任何交换，说明数组已经排序完毕，可以直接退出
        if (!swapped) {
            break;
        }
    }
} //end


