/**
    * 冒泡排序函数
    * aa bb cc
    *
            * @param a 待排序的数组
    * @param n 待排序的数组长度
    */
public static void bubbleSort(int[] a, int n) {
    // 你的代码，使无序数组 a 变得有序
    int i, j;
    for (i = 0; i < n; i++) {//表示n次排序过程。
        for (j = 1; j < n - i; j++) {
            if (a[j - 1] > a[j]) {//前面的数字大于后面的数字就交换
                //交换a[j-1]和a[j]
                int temp;
                temp = a[j - 1];
                a[j - 1] = a[j];
                a[j] = temp;
            }
        }
    }
} //end