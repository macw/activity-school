/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */

public static void bubbleSort(int [] a, int n) {
    for (int i = n; i > 1; --i) {
        for (int j = 1; j < i; ++j) {
	    if (a[j] < a[j-1]) {
	        final int temp = a[j];
		a[j] = a[j-1];
		a[j-1] = temp;
	    }
	}
    }
} // end
