/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for(int i = 0; i < n; i++) {
	    for(int yinrr = 0; yinrr < n - i - 1; yinrr++) {
		    if(a[yinrr] > a[yinrr + 1]) {
			    int temp = a[yinrr];
			    a [yinrr] = a[yinrr + 1];
			    a[yinrr + 1] = temp;
		    }
	    }
    }


} //end,yinrrzj
